import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadShoesHats() {
  const shoesResponse = await fetch('http://localhost:8080/api/shoes/');
  const hatsResponse = await fetch('http://localhost:8090/api/hats/');

  if (shoesResponse.ok && hatsResponse.ok) {
    const shoesData = await shoesResponse.json();
    const hatsData = await hatsResponse.json();

    console.log(shoesData)
    console.log(hatsData)

    root.render(
      <React.StrictMode>
        <App shoes={shoesData.shoes} hats={hatsData.hats} />
      </React.StrictMode>
    )
  } else{
    console.error(shoesResponse || hatsResponse)
  }
}

loadShoesHats()

// async function loadShoes() {
//   const response = await fetch('http://localhost:8080/api/shoes/');
//   if (response.ok) {
//     const data = await response.json();
//     console.log(data)

//     root.render(
//       <React.StrictMode>
//         <App shoes={data.shoes} />
//       </React.StrictMode>
//     )
//   } else{
//     console.error(response)
//   }
// }


// async function loadHats() {
//   const response = await fetch('http://localhost:8090/api/hats/');
//   if (response.ok) {
//     const data = await response.json();
//     console.log(data)

//     root.render(
//       <React.StrictMode>
//         <App hats={data.hats} />
//       </React.StrictMode>
//     )
//   } else{
//     console.error(response)
//   }
// }

// loadShoes()
// loadHats()
