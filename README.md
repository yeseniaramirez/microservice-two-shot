# Wardrobify

Team:

* Yesenia Ramirez - Shoes microservice
* Corey Edwards - Hats 

## Design

## Shoes microservice

I will install the shoes app into the wardrobe microservice and
make my models and views to show the list of my model. I will write 
the paths to my views and check my work on Insomnia. I will add to 
my view function to handle POST requests. Build a React component
to fetch and show the list in the Web page. I will poll the Bin data.
I will implement the delete request and add a delete button to finish
off the microservice. 

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
