import React from 'react';

function HatsList(props) {

    async function deleteHat(hat_id){
        const deleteUrl = `http://localhost:8090/api/hats/${hat_id}/`;
        const fetchConfig = {method: "delete"}        

        const response = await fetch(deleteUrl, fetchConfig);
        if (response.ok) {
            console.log(`we deleted it`, response)
        }
    };
    return (
        <table className="table">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.style }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.location }</td>
                            <td> <button onClick={() => deleteHat(hat.id)} type="button" className='btn btn-danger'>Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default HatsList;