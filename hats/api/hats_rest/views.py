from inspect import EndOfBlock
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from django.shortcuts import render
from common.json import ModelEncoder
from .models import Hats, LocationVO

# Create your views here.
class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name", 
        "section_number", 
        'shelf_number',
    ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style",
        "color",
        "id",
    ]
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style",
        "color",
        "picture",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )
        
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hats(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)

        return JsonResponse(
            {"hats": hat},
            encoder = HatsDetailEncoder,
        )
    
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location_href = content["location"]
                location = LocationVO.objects.get(import_href=location_href)
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400
            )
        Hats.objects.filter(id=pk).update(**content)
        shoe = Hats.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=HatsDetailEncoder,
            safe=False
        )